// receive.go
package main

import (
	"crypto/md5"
	"fmt"
	"html/template"
	"io"
	// "log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"./Lecturas"
)

func uploadHandler(w http.ResponseWriter, r *http.Request) {

	// the FormFile function takes in the POST input id file
	file, header, err := r.FormFile("file")

	if err != nil {
		fmt.Fprintln(w, err)
		return
	}

	defer file.Close()

	path, _ := os.Getwd()

	fmt.Println("directorio actual: " + path)

	out, err := os.Create(path + "archivosubido")
	if err != nil {
		fmt.Fprintf(w, "Unable to create the file for writing. Check your write access privilege", err)
		return
	}
	defer out.Close()

	// write the content from POST to the file
	_, err = io.Copy(out, file)
	if err != nil {
		fmt.Fprintln(w, err)
	}

	fmt.Fprintf(w, "File uploaded successfully : ")
	fmt.Fprintf(w, header.Filename)
}

func upload(w http.ResponseWriter, r *http.Request) {

	if r.Method == "GET" {
		crutime := time.Now().Unix()
		h := md5.New()
		io.WriteString(h, strconv.FormatInt(crutime, 10))
		token := fmt.Sprintf("%x", h.Sum(nil))

		t, _ := template.ParseFiles("upload.gtpl")
		t.Execute(w, token)
	} else if r.Method == "POST" {
		file, handler, err := r.FormFile("file")

		if err != nil {
			fmt.Println(err)
			return
		}

		if filepath.Ext(handler.Filename) == ".txt" {
			defer file.Close()
			//fmt.Fprintf(w, "%v", handler.Header)

			path, _ := os.Getwd()
			path = path + "/uploads/"
			fmt.Println("Directorio del archivo: ", path)
			if err != nil {
				fmt.Println("Ocurrio un error: ", err)
			}

			f, err := os.OpenFile(path+handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
			if err != nil {
				fmt.Println(err)
				return
			}
			defer f.Close()
			io.Copy(f, file)
			Lecturas.LecturaPrincipal(path + handler.Filename)
			fmt.Println("Mensaje: ", "Archivo cargado correctamente")
			fmt.Fprintf(w, "Archivo cargado correctamente")
		} else {
			fmt.Println("Extension de archivo invalido")
			fmt.Fprintf(w, "Extension de archivo no valido")
		}
	}
}

func main() {
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))
	http.HandleFunc("/", vista)
	http.HandleFunc("/receive", upload)
	fmt.Println("Servidor escuchando en la siguiente direccion:")
	fmt.Println("http://localhost:8084/")
	err := http.ListenAndServe(":8084", nil)
	if err != nil {
		fmt.Println("Ha ocurrido el siguiente error: ", err)
	}
}

func vista(w http.ResponseWriter, req *http.Request) {
	t, err := template.ParseFiles("Lecturas/basic_form.html")
	if err != nil {
		fmt.Println("Error al mostrar la vista")
	}
	t.Execute(w, nil)
}
