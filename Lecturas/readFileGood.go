// main2
package Lecturas

import (
	"bufio"
	"database/sql"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"

	_ "github.com/lib/pq"
)

type ParametrosConexion struct {
	host, nombreBD, usuarioDb, passwordDb string
}

func LecturaPrincipal(nameFile string) {
	fmt.Println("Archivo recibido: ", nameFile)
	parms := establecerParametros("localhost", "mtspay", "M1T3ch1t001", "mts01")
	con := ConexionDB(parms)
	readFile(con, nameFile)
}

func establecerParametros(ip, bd, psw, usr string) ParametrosConexion {
	p := ParametrosConexion{}
	p.host = ip
	p.nombreBD = bd
	p.passwordDb = psw
	p.usuarioDb = usr
	return p
}

func readFile(db *sql.DB, name string) {
	f, _ := os.Open(name)
	r := csv.NewReader(bufio.NewReader(f))

	r.Comma = '|'
	r.Comment = '#'

	x := 0
	for {
		records, err := r.Read()
		if x > 4 {
			if err == io.EOF {
				fmt.Println("Finaliza la lectura del archivo|Ocurre un error en el archivo: ", err)
				break
			}
			var fecha string = records[1]
			folio := records[9]
			insertDB(db, folio, fecha)
		}
		x++
	}
}

func insertDB(db *sql.DB, folio, fecha string) {
	existente := "SELECT folio, fecha from app_folios_pagados where folio = '" + folio + "' and fecha='" + fecha + "'"
	var num, fec string
	db.QueryRow(existente).Scan(&num, &fec)
	crear_tabla := "CREATE TABLE IF NOT EXISTS app_folios_pagados( folio character varying NOT NULL, fecha date, PRIMARY KEY (folio))"
	db.Query(crear_tabla)
	if num == "" {
		sql := "INSERT INTO app_folios_pagados values('" + folio + "','" + fecha + "');"
		_, err := db.Query(sql)
		if err != nil {
			fmt.Println("Error al realizar la insercion de datos")
			log.Fatal(err)
		}
	} else {
		fmt.Println("FOLIO DUPLICADO: " + num)
	}

}

func ConexionDB(param ParametrosConexion) *sql.DB {
	db, err := sql.Open("postgres", "host="+param.host+" user="+param.usuarioDb+" dbname="+param.nombreBD+" password="+param.passwordDb+" sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	return db
}
